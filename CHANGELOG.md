All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

### [0.1.0] 2024-07-03
 * Start semantic Versioning
 * Add camera script

