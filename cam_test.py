import cv2
import numpy as np

def put_text(frame, text):
    font = cv2.FONT_HERSHEY_SIMPLEX
    position = (15, 25)  # Posición (x, y) del texto
    font_scale = 0.7
    font_color = (255, 255, 255)  # Blanco
    thickness = 2
    line_type = cv2.LINE_AA

    cv2.putText(frame, text, position, font, font_scale, font_color, thickness, line_type)

    return frame


def start_camera(device):
    cap = cv2.VideoCapture(device)

    if cap.isOpened() == False:
        print("Error opening video file")

    while True:
        ret, frame = cap.read()
        if ret is not None:
            cv2.namedWindow('Mustela', cv2.WINDOW_NORMAL)
            cv2.setWindowProperty('Mustela', cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
            put_text(frame, 'Presione q para salir')
            cv2.imshow('Mustela', frame)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        else:
            break

    cap.release()
    cv2.destroyAllWindows()


if __name__ == '__main__':
    device = '/dev/video0'
    start_camera(device)
