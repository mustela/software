import RPi.GPIO as GPIO
import smbus
import time
from pyPS4Controller.controller import Controller

# Configuración de los pines GPIO
GPIO.setmode(GPIO.BOARD)

# Pines de salida
output_pins = {
    "pin_29": 29,
    "pin_31": 31,
    "pin_33": 33,
    "pin_37": 37,
}

# Configurar los pines de salida
GPIO.setup(output_pins["pin_29"], GPIO.OUT)
GPIO.setup(output_pins["pin_31"], GPIO.OUT)
GPIO.setup(output_pins["pin_33"], GPIO.OUT)
GPIO.setup(output_pins["pin_37"], GPIO.OUT)

# Establecer estados iniciales


# Dirección I2C del PCA9685
PCA9685_ADDRESS = 0x40

# Registros del PCA9685
MODE1 = 0x00
PRESCALE = 0xFE

# Frecuencia del PWM
PWM_FREQ = 50

# Configuración del bus I2C
bus = smbus.SMBus(1)  # 1 indica que estamos usando el bus I2C-1

# Inicialización del PCA9685
def pca9685_init():
    # Configurar el modo de operación del PCA9685
    bus.write_byte_data(PCA9685_ADDRESS, MODE1, 0x00)
    time.sleep(0.005)  # Esperar 5ms para que se establezca
    
    # Configurar la frecuencia del PWM
    prescale_value = int(25000000.0 / (4096 * PWM_FREQ) - 1)
    bus.write_byte_data(PCA9685_ADDRESS, MODE1, 0x10)  # Modo de reposo
    bus.write_byte_data(PCA9685_ADDRESS, PRESCALE, prescale_value)
    bus.write_byte_data(PCA9685_ADDRESS, MODE1, 0x80)  # Activar el modo de respuesta
    time.sleep(0.005)  # Esperar 5ms para que se establezca

# Establecer un ciclo de trabajo del 100%
def set_pwm(channel, on, off):
    bus.write_byte_data(PCA9685_ADDRESS, 0x06 + 4 * channel, on & 0xFF)
    bus.write_byte_data(PCA9685_ADDRESS, 0x07 + 4 * channel, on >> 8)
    bus.write_byte_data(PCA9685_ADDRESS, 0x08 + 4 * channel, off & 0xFF)
    bus.write_byte_data(PCA9685_ADDRESS, 0x09 + 4 * channel, off >> 8)

# Configuración inicial del PCA9685
pca9685_init()

# Configuración del pin 11 como salida (Output Enable del PCA9685)
GPIO.setup(11, GPIO.OUT)
GPIO.output(11, GPIO.LOW)  # Enable output

# Clase para manejar el controlador PS4
class JoyStick(Controller):
    def __init__(self, **kwargs):
        Controller.__init__(self, **kwargs)
        self.on_value = 0
        self.off_value = 0

    def on_L3_down(self, value):
        GPIO.output(output_pins["pin_29"], GPIO.LOW)
        GPIO.output(output_pins["pin_31"], GPIO.HIGH)
        GPIO.output(output_pins["pin_33"], GPIO.HIGH)
        GPIO.output(output_pins["pin_37"], GPIO.LOW)
        self.on_value = value
        set_pwm(0,self.off_value,int(0.124*self.on_value)) 
        set_pwm(1,self.off_value,int(0.124*self.on_value))

    def on_L3_up(self, value):
        GPIO.output(output_pins["pin_29"], GPIO.HIGH)
        GPIO.output(output_pins["pin_31"], GPIO.LOW)
        GPIO.output(output_pins["pin_33"], GPIO.LOW)
        GPIO.output(output_pins["pin_37"], GPIO.HIGH)
        self.on_value = value
        set_pwm(0, self.off_value, int((-1*0.124)*self.on_value))
        set_pwm(1, self.off_value, int((-1*0.124)*self.on_value))
    
    def on_R3_down(self, value):
        GPIO.output(output_pins["pin_29"], GPIO.LOW)
        GPIO.output(output_pins["pin_31"], GPIO.HIGH)
        GPIO.output(output_pins["pin_33"], GPIO.LOW)
        GPIO.output(output_pins["pin_37"], GPIO.HIGH)
        self.on_value = value
        set_pwm(0,self.off_value,int(0.124*self.on_value)) 
        set_pwm(1,self.off_value,int(0.124*self.on_value))

    def on_R3_up(self, value):
        GPIO.output(output_pins["pin_29"], GPIO.HIGH)
        GPIO.output(output_pins["pin_31"], GPIO.LOW)
        GPIO.output(output_pins["pin_33"], GPIO.HIGH)
        GPIO.output(output_pins["pin_37"], GPIO.LOW)
        self.on_value = value
        set_pwm(0, self.off_value, int(-0.124*self.on_value))
        set_pwm(1, self.off_value, int(-0.124*self.on_value))
    
  
    


if __name__ == '__main__':
    controller = JoyStick(interface="/dev/input/js0", connecting_using_ds4drv=False)
    
    try:
        controller.listen(timeout=60)
    except KeyboardInterrupt:
        pass
    finally:
        GPIO.cleanup()
