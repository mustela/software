import smbus
import time
import sys
from pyPS4Controller.controller import Controller

# Dirección I2C del PCA9685
PCA9685_ADDRESS = 0x40

# Registro de los comandos del PCA9685
MODE1 = 0x00
PRESCALE = 0xFE
LED0_ON_L = 0x06
LED0_OFF_L = 0x08

# Frecuencia deseada (50Hz para controlar el ESC)
FREQUENCY = 50

# Inicializa el bus I2C
bus = smbus.SMBus(1)  # El bus I2C en Raspberry Pi (1 para la mayoría de los modelos)


class JoyStick(Controller):
    sens=[0.5,0.006,200] #Sensibilidad velocidad/giro, Ajuste de valores de paramentro, Valor punto muerto
    velo = 0
    turn = [0,0]
    Dleft= False        #Variable de control direccion de giro lado izquierdo
    Dright = False        #Variable de control dirrecion de giro lado dercho
    enable = False      

    def __init__(self, **kwargs):
        Controller.__init__(self, **kwargs)
     

    def on_L3_down(self, value):
        self.velo = int(abs(value)*bool(value>500))
        self.Dleft = True
        self.Dright = True
        self.set_pwmo()
    
    def on_L3_up(self, value):
        self.velo = int(abs(value)*bool(abs(value)>500))
        self.Dleft = False
        self.Dright = False
        self.set_pwmo()
    
    def on_R3_left(self, value):
        self.turn[0] = int(abs(value)*bool(abs(value)>500))
        self.turn[1] = 0
        if (self.velo==0):
            self.Dleft= True
            self.Dright = False
        self.set_pwmo()

    def on_R3_right(self, value):
        self.turn[1] = int(abs(value)*bool(abs(value)>500))
        self.turn[0] = 0
        if (self.velo==0):
            self.Dleft = False
            self.Dright = True
        self.set_pwmo()
    
    def on_R1_press(self):
        self.enable = True
        self.set_pwmo()

    def on_R1_release(self):
        self.enable = False
        self.set_pwmo()

    def set_pwmo(self):
        

        controlL = self.sens[1] * (abs(self.velo * self.sens[0] + (self.turn[1]-self.turn[0])* (1 - self.sens[0]))) + self.sens[2]
        controlR = self.sens[1] * (abs(self.velo * self.sens[0] + (self.turn[0]-self.turn[1]) * (1 - self.sens[0]))) + self.sens[2]
        set_pwm(0, 0, int(self.enable * controlL))
        set_pwm(1, 0, int(self.enable * controlR))

        print("TurnL:",self.turn[0],"TurnR:",self.turn[1],"Veolcity:",self.velo,"DircI:",self.Dleft,"Dirc:",self.Dright)
    
    def set_pwcam(self):
        return 0

def set_pwm_freq(frequency):
    """
    Configura la frecuencia PWM en el PCA9685.
    """
    prescale_value = int(25000000.0 / (4096.0 * frequency) - 1)
    old_mode = bus.read_byte_data(PCA9685_ADDRESS, MODE1)
    new_mode = (old_mode & 0x7F) | 0x10  # Entrar en modo de reposo
    bus.write_byte_data(PCA9685_ADDRESS, MODE1, new_mode)
    bus.write_byte_data(PCA9685_ADDRESS, PRESCALE, prescale_value)
    bus.write_byte_data(PCA9685_ADDRESS, MODE1, old_mode)
    time.sleep(0.005)
    bus.write_byte_data(PCA9685_ADDRESS, MODE1, old_mode | 0x80)  # Reiniciar

def set_pwm(channel, on, off):
    """
    Establece el valor PWM para un canal específico.
    """
    bus.write_byte_data(PCA9685_ADDRESS, LED0_ON_L + 4 * channel, on & 0xFF)
    bus.write_byte_data(PCA9685_ADDRESS, LED0_ON_L + 4 * channel + 1, on >> 8)
    bus.write_byte_data(PCA9685_ADDRESS, LED0_OFF_L + 4 * channel, off & 0xFF)
    bus.write_byte_data(PCA9685_ADDRESS, LED0_OFF_L + 4 * channel + 1, off >> 8)



if __name__ == "__main__":
    controller = JoyStick(interface="/dev/input/js0", connecting_using_ds4drv=False)
    
    try:
        controller.listen(timeout=60)
    except KeyboardInterrupt:
        pass
    finally:
        GPIO.cleanup()
