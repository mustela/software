import RPi.GPIO as GPIO
import smbus
import time
from pyPS4Controller.controller import Controller

# Configuración de los pines GPIO
GPIO.setmode(GPIO.BOARD)

# Configurar los pines de salida
GPIO.setup(29, GPIO.OUT)
GPIO.setup(31, GPIO.OUT)
GPIO.setup(33, GPIO.OUT)
GPIO.setup(37, GPIO.OUT)


# Dirección I2C del PCA9685
PCA9685_ADDRESS = 0x40

# Registros del PCA9685
MODE1 = 0x00
PRESCALE = 0xFE

# Frecuencia del PWM
PWM_FREQ = 50

# Configuración del bus I2C
bus = smbus.SMBus(1)  # 1 indica que estamos usando el bus I2C-1

# Inicialización del PCA9685
def pca9685_init():
    # Configurar el modo de operación del PCA9685
    bus.write_byte_data(PCA9685_ADDRESS, MODE1, 0x00)
    time.sleep(0.005)  # Esperar 5ms para que se establezca
    
    # Configurar la frecuencia del PWM
    prescale_value = int(25000000.0 / (4096 * PWM_FREQ) - 1)
    bus.write_byte_data(PCA9685_ADDRESS, MODE1, 0x10)  # Modo de reposo
    bus.write_byte_data(PCA9685_ADDRESS, PRESCALE, prescale_value)
    bus.write_byte_data(PCA9685_ADDRESS, MODE1, 0x80)  # Activar el modo de respuesta
    time.sleep(0.005)  # Esperar 5ms para que se establezca

# Establecer un ciclo de trabajo del 100%
def set_pwm(channel, on, off):
    bus.write_byte_data(PCA9685_ADDRESS, 0x06 + 4 * channel, on & 0xFF)
    bus.write_byte_data(PCA9685_ADDRESS, 0x07 + 4 * channel, on >> 8)
    bus.write_byte_data(PCA9685_ADDRESS, 0x08 + 4 * channel, off & 0xFF)
    bus.write_byte_data(PCA9685_ADDRESS, 0x09 + 4 * channel, off >> 8)

# Configuración inicial del PCA9685
pca9685_init()

# Configuración del pin 11 como salida (Output Enable del PCA9685)
GPIO.setup(11, GPIO.OUT)
GPIO.output(11, GPIO.LOW)  # Enable output

# Clase para manejar el controlador PS4
class JoyStick(Controller):
    sens=[0.6,0.1,250] #Sensibilidad velocidad/giro, Ajuste de valores de paramentro, Valor punto muerto
    velo = 0
    turn = [0,0]
    Dleft= False        #Variable de control direccion de giro lado izquierdo
    Dright = False        #Variable de control dirrecion de giro lado dercho
    enable = False      

    def __init__(self, **kwargs):
        Controller.__init__(self, **kwargs)
     

    def on_L3_down(self, value):
        self.velo = int(abs(value)*bool(value>500))
        self.Dleft = True
        self.Dright = True
        self.set_pwmo()
    
    def on_L3_up(self, value):
        self.velo = int(abs(value)*bool(abs(value)>500))
        self.Dleft = False
        self.Dright = False
        self.set_pwmo()
    
    def on_R3_left(self, value):
        self.turn[0] = int(abs(value)*bool(abs(value)>500))
        self.turn[1] = 0
        if (self.velo==0):
            self.Dleft= True
            self.Dright = False
        self.set_pwmo()

    def on_R3_right(self, value):
        self.turn[1] = int(abs(value)*bool(abs(value)>500))
        self.turn[0] = 0
        if (self.velo==0):
            self.Dleft = False
            self.Dright = True
        self.set_pwmo()
    
    def on_R1_press(self):
        self.enable = True
        self.set_pwmo()

    def on_R1_release(self):
        self.enable = False
        self.set_pwmo()

    def set_pwmo(self):
        GPIO.output(29, self.Dleft)
        GPIO.output(31, not self.Dleft)
        GPIO.output(33, not self.Dright)
        GPIO.output(37, self.Dright)

        controlL = self.sens[1] * (abs(self.velo * self.sens[0] + (self.turn[1]-self.turn[0])* (1 - self.sens[0]))) + self.sens[2]
        controlR = self.sens[1] * (abs(self.velo * self.sens[0] + (self.turn[0]-self.turn[1]) * (1 - self.sens[0]))) + self.sens[2]
        set_pwm(0, 0, int(self.enable * controlR))
        set_pwm(1, 0, int(self.enable * controlL))

        print("TurnL:",self.turn[0],"TurnR:",self.turn[1],"Veolcity:",self.velo,"DircI:",self.Dleft,"Dirc:",self.Dright)
    
    def set_pwcam(self):
        return 0





if __name__ == '__main__':
    controller = JoyStick(interface="/dev/input/js0", connecting_using_ds4drv=False)
    
    try:
        controller.listen(timeout=60)
    except KeyboardInterrupt:
        pass
    finally:
        GPIO.cleanup()
